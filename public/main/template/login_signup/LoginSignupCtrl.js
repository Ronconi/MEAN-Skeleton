'use strict'
angular.module('app.controllers')
.controller('LoginSignupCtrl', function($rootScope, $scope, UserService, StorageService, $location, ngDialog) {

  $scope.user = {};
  $scope.error = null;
  $scope.msg = null;
  $scope.loading = false;

  var today = new Date();
  var minAge = 18;
  $scope.minAge = new Date(today.getFullYear() - minAge, today.getMonth(), today.getDate());

  $scope.login = function(isValid){
    $scope.isSubmitted = true;
    if(!isValid) return;
    //create user for login
    $scope.loading = true;
    var user = {
      email: $scope.user.email,
      password: $scope.user.password
    };
    //call login
    UserService.login($scope.user).then(function(res){
      if(res.data.success){
        //modal
        var dialog = ngDialog.open({
          template: 'main/template/modals/loginSuccess.html',
          className: 'ngdialog-theme-default'
        });
        dialog.closePromise.then(function (data) {
          StorageService.setUser(res.data.user, res.data.token);
          $rootScope.isLogged = true;
          $location.path('/');
          $scope.user = {};
        });
        // end modal
      }else{
        $scope.error = true;
        $scope.msg = res.data.msg;
      }
    }).catch(function(err){
      console.log("CATHCATO");
        var dialogError = ngDialog.open({
          template: 'main/template/modals/errorModal.html',
          className: 'ngdialog-theme-default'
        });
        dialogError.closePromise.then(function (data) {
            $location.path('/');
            $scope.user = {};
        });
    }).finally(function(){
        $scope.loading = false;
    })
  }

  $scope.signup = function(isValid){
    $scope.isSubmitted = true;
    if(!isValid) return;
      //create user for login after signing up
      var user = {
        email: $scope.user.email,
        password: $scope.user.password
      };
      //call signup
      UserService.signup($scope.user).then(function(res){
        if(res.data.success){
          $scope.login(true);
        }else{
          $scope.error = true;
          $scope.msg = res.data.msg;
        }
      });
  }

});
