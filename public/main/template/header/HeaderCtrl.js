'use strict'
angular.module('app.controllers')
.controller('HeaderCtrl', function($http, $rootScope, $scope, $location, $translate, UserService, StorageService, $window){

  $rootScope.isLogged = UserService.isLogged();
  var lang = $translate.use();

  $scope.langs = [
    {
      name: "Italiano",
      short: "it"
    },
    {
      name: "English",
      short: "en"
    }
  ];

  $scope.langs.forEach(function(val){
    if(val.short == lang)
      $scope.lang = val;
  });

  $scope.menuItemLeft = [
    {
      name: "Home",
      url: "/",
      visibleAfterLogin: true,
      visibleBeforeLogin: true,
    }
  ];

  $scope.menuItemRight = [
    {
      name: "Login",
      url: "/login",
      visibleAfterLogin: false,
      visibleBeforeLogin: true,
    },
    {
      name: "Signup",
      url: "/signup",
      visibleAfterLogin: false,
      visibleBeforeLogin: true,
    },
    {
      name: "Logout",
      url: "/",
      visibleAfterLogin: true,
      visibleBeforeLogin: false,
      func: 'logout()'
    }
  ];

  $scope.isActive = function (viewLocation) {
    return viewLocation === $location.path();
  };

  $scope.isVisible = function (item){
    if($rootScope.isLogged){
      if(item.visibleAfterLogin)
        return true;
      else
        return false;
    } else {
      if(item.visibleBeforeLogin)
        return true;
      else
        return false;
    }
  }

  $scope.logout = function(){
    $rootScope.isLogged = false;
    $scope.logoutVisible = false;
    StorageService.unsetUser();
    $location.path("/");
    $window.location.reload();
  }

   $scope.subreddits = ['cats', 'pics', 'funny', 'gaming', 'AdviceAnimals', 'aww'];

  $scope.changeLanguage = function (key) {
    $translate.use(key);
    $location.path('/');
    $window.location.reload();
  };

});
