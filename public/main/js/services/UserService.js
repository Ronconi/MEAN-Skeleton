angular.module('app.services')
.factory('UserService', function($http, Root, StorageService, $translate) {
  'use strict';

  var lang = $translate.use();

  var UserService = {

    login: function(user){
      var req = {
        method: 'POST',
        url: Root.url + "auth/normal",
        headers: {
          'Accept-Language': lang
        },
        data: {email: user.email, password: user.password}
      }
      return $http(req);
    },

    signup: function(user){
      var req = {
        method: 'POST',
        url: Root.url + "signup/normal",
        headers: {
          'Accept-Language': lang
        },
        data: {
          email: user.email,
          password: user.password,
          name: user.name,
          lastname: user.lastname,
          address: user.address,
          birthdate: user.birthdate,
          phone: user.phone,
          addressNum: user.addressNum,
          ring: user.ring
        }
      }
      return $http(req);
    },

    isLogged: function(){
      return StorageService.getUser() !== undefined;
    }

  };

  return UserService;
});
