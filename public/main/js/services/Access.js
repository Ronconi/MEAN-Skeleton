angular.module('app.services')
.factory("Access", function ($q, UserService) {

  var Access = {
    OK: 200,
    UNAUTHORIZED: 401,
    FORBIDDEN: 403,

    isAnonymous: function () {
      return $q(function (resolve, reject) {
        if (!UserService.isLogged())
          resolve(Access.OK);
        else
          reject(Access.UNAUTHORIZED);
      });
    },

    isAuthenticated: function () {
      return $q(function (resolve, reject) {
        if (UserService.isLogged()) 
          resolve(Access.OK);
        else
          reject(Access.UNAUTHORIZED);
      });
    }

  };

  return Access;

});
