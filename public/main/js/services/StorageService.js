angular.module('app.services')
.factory('StorageService', function($localStorage) {
  'use strict';
  if($localStorage.main === undefined)
    $localStorage.main = {};
  var service = {
    setUser: function(user, token){
      $localStorage.main.user = user;
      $localStorage.main.token = token;
    },
    unsetUser: function(){
      $localStorage.main.token = undefined;
      $localStorage.main.user = undefined;
    },
    getUser: function(){
      return $localStorage.main.user;
    }
  };

  return service;
});
