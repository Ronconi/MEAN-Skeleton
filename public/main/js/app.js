angular.module('app', [
  'app.controllers',
  'app.services',
  'ngRoute',
  'pascalprecht.translate',
  'ngStorage',
  'ngCookies',
  'ngAnimate',
  'ngMessages',
  'angular-loading-bar',
  'ngDialog'
])
.constant("Root", {
     "url": "http://localhost:3000/"
})

.run(function($rootScope, Access, $location){
  $rootScope.$on("$routeChangeError", function (event, current, previous, rejection) {
    if (rejection == Access.UNAUTHORIZED) {
      $location.path("/");
    } else if (rejection == Access.FORBIDDEN) {
      $location.path("/");
    }
  });
})

.config(function(cfpLoadingBarProvider, Root, $routeProvider, $locationProvider, $translateProvider) {

  cfpLoadingBarProvider.includeSpinner = true;

  $translateProvider.registerAvailableLanguageKeys(['en', 'it']);
  $translateProvider.useStaticFilesLoader({
      prefix: Root.url + "resources/locale-",
      suffix: ".json"
  });
  $translateProvider.useLocalStorage();
  $translateProvider.determinePreferredLanguage();
  $translateProvider.useSanitizeValueStrategy('sanitizeParameters');
  $locationProvider.html5Mode(true);

  $routeProvider
    .when("/", {
      templateUrl: "main/template/home/home.html",
      controller: "HomeCtrl"
    })
    .when("/login", {
      templateUrl: "main/template/login_signup/login.html",
      controller: "LoginSignupCtrl",
      resolve: {
        access: ["Access", function (Access) { return Access.isAnonymous(); }]
      }
    })
    .when("/signup", {
      templateUrl: "main/template/login_signup/signup.html",
      controller: "LoginSignupCtrl",
      resolve: {
        access: ["Access", function (Access) { return Access.isAnonymous(); }]
      }
    })
    .otherwise({ redirectTo: '/' });
});
