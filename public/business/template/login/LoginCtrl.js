'use strinct'
angular.module('app.controllers')
.controller('LoginCtrl', function($rootScope, $scope, UserService, StorageService, $location, ngDialog) {

  $scope.email = "";
  $scope.password = "";

  $scope.user = {};
  $scope.error = null;
  $scope.msg = null;
  $scope.loading = false;

  $scope.login = function(isValid){
    $scope.isSubmitted = true;
    if(!isValid) return;
    //create user for login
    $scope.loading = true;
    var user = {
      email: $scope.user.email,
      password: $scope.user.password
    };
    //call login
    UserService.login($scope.user).then(function(res){
      if(res.data.success){
        //modal
        var dialog = ngDialog.open({
          template: 'business/template/modals/loginSuccess.html',
          className: 'ngdialog-theme-default'
        });
        dialog.closePromise.then(function (data) {
          StorageService.setUser(res.data.user, res.data.token);
          $rootScope.isLogged = true;
          $location.path('/business');
          $scope.user = {};
        });
        // end modal
      }else{
        $scope.error = true;
        $scope.msg = res.data.msg;
      }
    }).catch(function(err){
        var dialog = ngDialog.open({
          template: 'business/template/modals/errorModal.html',
          className: 'ngdialog-theme-default'
        });
        dialog.closePromise.then(function (data) {
            $location.path('/business');
            $scope.user = {};
        });
    }).finally(function(){
        $scope.loading = false;
    })
  }

});
