angular.module('app.services')
.factory('UserService', function($translate, $http, Root, StorageService) {
  'use strict';
  var lang = $translate.use();
  var UserService = {
    login: function(user){
      var req = {
        method: 'POST',
        url: Root.url + "auth/business",
        headers: {
          'Accept-Language': lang
        },
        data: {email: user.email, password: user.password}
      }
      return $http(req);
    },
    isLogged: function(){
      return StorageService.getUser() !== undefined;
    }
  };

  return UserService;
});
