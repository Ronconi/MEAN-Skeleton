angular.module('app.services')
.factory('StorageService', function($localStorage) {
  'use strict';
  if($localStorage.business === undefined)
    $localStorage.business = {};
  var service = {
    setUser: function(user, token){
      $localStorage.business.user = user;
      $localStorage.business.token = token;
    },
    unsetUser: function(){
      $localStorage.business.token = undefined;
      $localStorage.business.user = undefined;
    },
    getUser: function(){
      return $localStorage.business.user;
    }
  };

  return service;
});
