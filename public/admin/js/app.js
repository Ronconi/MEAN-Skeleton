angular.module('app', [
  'app.controllers',
  'app.services',
  'ngRoute',
  'pascalprecht.translate',
  'ngStorage',
  'ngCookies',
  'ngAnimate',
  'ngMessages',
  'angular-loading-bar',
  'ngDialog'
])
.constant("Root", {
     "url": "http://localhost:3000/"
})

.run(function($rootScope, Access, $location){
  $rootScope.$on("$routeChangeError", function (event, current, previous, rejection) {
    if (rejection == Access.UNAUTHORIZED) {
      $location.path("/admin/login");
    } else if (rejection == Access.LOGGED) {
      $location.path("/admin");
    }
  });
})

.config(function(Root, $routeProvider, $locationProvider, $translateProvider) {

  $translateProvider.registerAvailableLanguageKeys(['en', 'it']);
  $translateProvider.useStaticFilesLoader({
      prefix: Root.url + "resources/locale-",
      suffix: ".json"
  });
  $translateProvider.useLocalStorage();
  $translateProvider.determinePreferredLanguage();
  $translateProvider.useSanitizeValueStrategy('escaped')
  $locationProvider.html5Mode(true);

  $routeProvider
    .when("/admin", {
      templateUrl: "admin/template/home/home.html",
      controller: "HomeCtrl",
      resolve: {
        access: ["Access", function (Access) { return Access.isAuthenticated(); }]
      }
    })
    .when("/admin/login", {
      templateUrl: "admin/template/login/login.html",
      controller: "LoginCtrl",
      resolve: {
        access: ["Access", function (Access) { return Access.isAnonymous(); }]
      }
    })
    .otherwise({ redirectTo: '/admin' });
});
