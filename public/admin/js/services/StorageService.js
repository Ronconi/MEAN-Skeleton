angular.module('app.services')
.factory('StorageService', function($localStorage) {
  'use strict';
  if($localStorage.admin === undefined)
    $localStorage.admin = {};
  var service = {
    setUser: function(user, token){
      $localStorage.admin.user = user;
      $localStorage.admin.token = token;
    },
    unsetUser: function(){
      $localStorage.admin.token = undefined;
      $localStorage.admin.user = undefined;
    },
    getUser: function(){
      return $localStorage.admin.user;
    }
  };

  return service;
});
