var JwtStrategy = require('passport-jwt').Strategy;
var FacebookStrategy = require('passport-facebook').Strategy;

var ExtractJwt = require('passport-jwt').ExtractJwt;
var user = require('../models/user.js');
var Admin = require('../models/admin.js');
var Business = require('../models/business.js');
var config = require('../config/config');

var facebook = config.facebook;
var jwt = require('jwt-simple');

module.exports = function(passport) {
  var opts = {};
  opts.jwtFromRequest = ExtractJwt.fromAuthHeader();
  opts.secretOrKey = config.secret;

  //use jwt strategy for user
  passport.use('jwt-user', new JwtStrategy(opts, function(jwt_payload, done){
    user.find(jwt_payload._id, function(err, user) {
      if(err)
        return done(err, false);
      if(user)
        done(null, user);
      else
        done(null, false);
    })
  }));

  //use jwt strategy for admin
  passport.use('jwt-admin', new JwtStrategy(opts, function(jwt_payload, done){
    admin.find(jwt_payload._id, function(err, user) {
      if(err)
        return done(err, false);
      if(user)
        done(null, user);
      else
        done(null, false);
    })
  }));

  //use jwt strategy for business
  passport.use('jwt-business', new JwtStrategy(opts, function(jwt_payload, done){
    business.find(jwt_payload._id, function(err, user) {
      if(err)
        return done(err, false);
      if(user)
        done(null, user);
      else
        done(null, false);
    })
  }));

  //use facebook strategy
  passport.use('fb-user', new FacebookStrategy({
    clientID: facebook.clientID,
    clientSecret: facebook.clientSecret,
    callbackURL: facebook.callbackURL
  },
  function(accessToken, refreshToken, profile, done){
    process.nextTick(function(){
      User.findOne({facebook_id: profile.id}, function(err, user){
        if(err)
          return done(err);
        if(user) {
          return done(null, user);
        } else {
          var newUser = new User();
          newUser.facebook_id = profile.id;
          newUser.name = profile.displayName;
          newUser.strategy = 'facebook';
          newUser.group = 'user';
          //newUser.facebook_email = profile.emails[0].value;
          //save fb user
          newUser.save(function(err){
            if(err)
              throw err;
            return done(null, newUser);
          })
        }
      });
    })
  }));
};
