var MongoClient = require('mongodb').MongoClient
var config = require('../config/config.js');

var url = config.database;

exports.connect = function(done) {
  MongoClient.connect(url, function(err, dbistance) {
    if (err) return done(err, null)
    exports.db = dbistance
    done(null, dbistance)
  })
}

exports.close = function(done) {
  if (db) {
    db.close(function(err, result) {
      db = null
      done(err)
    })
  }
}

exports.clear = function(done) {
  db.dropDatabase(function(err){
    done()
  })
}
