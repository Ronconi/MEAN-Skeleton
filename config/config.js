
switch(process.env.NODE_ENV){
  case 'development':
    var database = process.env.DB || 'mongodb://localhost:27017/db_test';
    var secret = 'yourSecret';
    var fb_clientID = process.env.FACEBOOK_ID || 'INSERT YOUR CLIENT ID';
    var fb_clientSecret = process.env.FACEBOOK_SECRET || 'INSERT YOUR CLIENT SECRET';
    break;
  case 'production':
    var database = process.env.DB || 'mongodb://localhost:27017/db_production';
    var secret = 'yourSecret';
    var fb_clientID = process.env.FACEBOOK_ID || 'INSERT YOUR CLIENT ID';
    var fb_clientSecret = process.env.FACEBOOK_SECRET || 'INSERT YOUR CLIENT SECRET';
    break;
}

module.exports = {
  secret: secret,
  database: database,
  facebook: {
    clientID: fb_clientID,
    clientSecret: fb_clientSecret,
    callbackURL: 'http://localhost:3000/auth/facebook/callback'
  }
};
