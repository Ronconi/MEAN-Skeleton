var passport = require('passport');
var user = require('../models/user.js');
var business = require('../models/business.js');
var admin = require('../models/admin.js');

var Auth = function(group) {
  switch(group){
    case 'normal':
      return [
        passport.authenticate('jwt-user', {session: false}),
        function(req, res, next) {
          var user = req.user;
          if(user)
            next();
          else
            res.send(401, 'Unauthorized');
        }
      ]
      break;
    case 'admin':
      return [
        passport.authenticate('jwt-admin', {session: false}),
        function(req, res, next) {
          var user = req.user;
          if(user)
            next();
          else
            res.send(401, 'Unauthorized');
        }
      ]
      break;
    case 'business':
      return [
        passport.authenticate('jwt-business', {session: false}),
        function(req, res, next) {
          var user = req.user;
          if(user)
            next();
          else
            res.send(401, 'Unauthorized');
        }
      ]
      break;
  }
  /*return [
    passport.authenticate('jwt', {session: false}),
    function(req, res, next) {
      var user = req.user;
      if(user.group == group || group == 'all')
        next();
      else
        res.send(401, 'Unauthorized');
    }
  ];*/
}

module.exports = {
  Auth: Auth
};
