var db = require('../config/db.js').db;
var ObjectId = require('mongodb').ObjectID;

var insert = function(user, callback){
  db.collection('users').findOne({ email: user.email }, function(err, u){
    if(err){
      callback(err, null);
    } else {
      if(u){
        var error = {
          code:11000,
          msg: "Duplicate Email"
        };
        callback(error, null);
      } else {
          db.collection('users').insertOne(user, function(err, user){
            if(err)
              callback(err, null);
            else
              callback(null, user);
          });
        }
      }
  })
};

var find = function(id, callback){
  console.log("ID: " + id);
  db.collection('users').findOne({_id: new ObjectId(id)}, function(err, u){
    if(err){
      callback(err, null)
    } else {
      if(u)
        callback(null, u)
      else
        callback(null, null)
    }
  })
}

var getAll = function(callback){
  db.collection('users').find({}, function(err, user){
    if(err)
      callback(err, null);
    else
      callback(null, user);
  });
}

var findByEmail = function(email, callback){
  db.collection('users').findOne({email: email}, function(err, user){
    if(err)
      callback(err, null);
    else
      callback(null, user);
  });
}

module.exports = {
  insert: insert,
  findByEmail: findByEmail,
  getAll: getAll,
  find: find
};
