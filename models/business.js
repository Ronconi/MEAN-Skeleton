var db = require('../config/db.js').db;
var ObjectId = require('mongodb').ObjectID;

var insert = function(user, callback){
  db.collection('business').findOne({ email: user.email }, function(err, u){
    if(err){
      callback(err, null);
    } else {
      if(u){
        var error = {
          code:11000,
          msg: "Duplicate Email"
        };
        callback(error, null);
      } else {
          db.collection('users').insertOne(user, function(err, user){
            if(err)
              callback(err, null);
            else
              callback(null, user);
          });
        }
      }
  })
};

var find = function(id, callback){
  db.collection('business').findOne({_id: new ObjectId(id)}, function(err, u){
    if(err){
      callback(err, null)
    } else {
      console.log(u);
      if(u)
        callback(null, u)
      else
        callback(null, null)
    }
  })
}

var getAll = function(callback){
  db.collection('business').find({}, function(err, user){
    if(err)
      callback(err, null);
    else
      callback(null, user);
  });
}

var findByEmail = function(id, done){
  db.collection('business').findOne({email: email}, function(err, user){
    if(err)
      callback(err, null);
    else
      callback(null, user);
  });
}

module.exports = {
  insert: insert,
  findByEmail: findByEmail,
  getAll: getAll,
  find: find
};
