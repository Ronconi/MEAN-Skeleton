var express = require('express');
var router = express.Router();
var controller = require('../controllers/signup');
var middlewares = require('../config/middlewares');

router.route('/normal')
  .post(
    controller.signup
  );

router.route('/admin')
  .post(
    middlewares.Auth('admin'),
    controller.signup
  );

router.route('/business')
  .post(
    middlewares.Auth('admin'),
    controller.signup
  );

module.exports = router;
