var express = require('express');
var router = express.Router();
var User = require('../models/user.js');
var controller = require('../controllers/auth');
var middlewares = require('../config/middlewares');
var jwt = require('jwt-simple');
var secret = require('../config/config').secret;
var passport = require('passport');

router.route('/normal')
  .post(
    controller.authenticate
  );

router.route('/admin')
  .post(
    controller.authenticate
  );

router.route('/business')
  .post(
    controller.authenticate
  );

module.exports = router;
