var express = require('express');
var router = express.Router();
var path = require('path');

/* GET ADMIN PANEL. */
router.get('/admin', function(req, res, next) {
  res.sendFile(path.join(__dirname, '../', "public/admin", 'index.html'));
});

/* GET BUSINESS PANEL. */
router.get('/admin/*', function(req, res, next) {
  res.sendFile(path.join(__dirname, '../', "public/admin", 'index.html'));
});

/* GET BUSINESS PANEL. */
router.get('/business', function(req, res, next) {
  res.sendFile(path.join(__dirname, '../', "public/business", 'index.html'));
});

/* GET BUSINESS PANEL. */
router.get('/business/*', function(req, res, next) {
  res.sendFile(path.join(__dirname, '../', "public/business", 'index.html'));
});

/* GET SITE. */
router.get('/', function(req, res, next) {
  res.sendFile(path.join(__dirname, '../', "public/main", 'index.html'));
});

router.get('/*', function(req, res, next) {
  res.sendFile(path.join(__dirname, '../', "public/main", 'index.html'));
});

module.exports = router;
