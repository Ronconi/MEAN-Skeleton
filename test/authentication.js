/*
var expect    = require("chai").expect;
var chai = require('chai');
var chaiHttp = require('chai-http');
var request = require("request");
var app = require('../app');
chai.use(chaiHttp);

describe("Authentication", function() {

  describe("Normal Authentication", function() {

    it("returns status 200", function(done) {
      chai.request(app)
      .post('/auth/normal')
      .then(function (res){
        expect(res).to.have.status(200);
        done();
      })
    });

    it("Authentication Failed", function(done) {
      chai.request(app)
      .post('/auth/normal')
      .send({email:"test@test.it", password: "asdafafa"})
      .then(function (res){
        var success = res.body.success;
        expect(success).to.equal(false);
        done();
      })
    });

    it("NORMAL SIGNUP", function(done){
      chai.request(app)
      .post('/signup/normal')
      .send(
        {
          name:"Nick",
          lastname:"Test",
          phone:"0220349586",
          address:"street 4",
          birthdate:"10/10/1980",
          email:"nick@gmail.com",
          password:"asdafafa"
        }
      )
      .end(function (err, res){
        var success = res.body.success;
        expect(err).to.be.null;
        expect(success).to.equal(true);
        done();
      })
    });

    it("Authentication Successful", function(done) {
      chai.request(app)
      .post('/auth/normal')
      .send(
        {
          email:"nick@gmail.com",
          password:"asdafafa"
        }
      )
      .end(function (err, res){
        var success = res.body.success;
        expect(err).to.be.null;
        expect(success).to.equal(true);
        done();
      })
    });

    it("Authentication Without Body", function(done) {
      chai.request(app)
      .post('/auth/normal')
      .end(function(err, res){
        var success=res.body.success;
        expect(success).to.equal(false);
        done();
      })
    });

  });

  describe("Admin Authentication", function() {

    it("returns status 200", function(done) {
      chai.request(app)
      .post('/auth/admin')
      .then(function (res){
        expect(res).to.have.status(200);
        done();
      })
    });

    it("Authentication Failed", function(done) {
      chai.request(app)
      .post('/auth/admin')
      .send({email:"test@test.it", password: "asdafafa"})
      .then(function (res){
        var success = res.body.success;
        expect(success).to.equal(false);
        done();
      })
    });

    it("admin SIGNUP", function(done){
      chai.request(app)
      .post('/signup/admin')
      .send(
        {
          name:"Nick",
          lastname:"Test",
          phone:"0220349586",
          address:"street 4",
          birthdate:"10/10/1980",
          email:"nick@gmail.com",
          password:"asdafafa"
        }
      )
      .end(function (err, res){
        var success = res.body.success;
        expect(err).to.be.null;
        expect(success).to.equal(true);
        done();
      })
    });

    it("Authentication Successful", function(done) {
      chai.request(app)
      .post('/auth/admin')
      .send(
        {
          email:"nick@gmail.com",
          password:"asdafafa"
        }
      )
      .end(function (err, res){
        var success = res.body.success;
        expect(err).to.be.null;
        expect(success).to.equal(true);
        done();
      })
    });

    it("Authentication Without Body", function(done) {
      chai.request(app)
      .post('/auth/admin')
      .end(function(err, res){
        var success=res.body.success;
        expect(success).to.equal(false);
        done();
      })
    });

  });

  describe("Business Authentication", function() {

    it("returns status 200", function(done) {
      chai.request(app)
      .post('/auth/business')
      .then(function (res){
        expect(res).to.have.status(200);
        done();
      })
    });

    it("Authentication Failed", function(done) {
      chai.request(app)
      .post('/auth/business')
      .send({email:"test@test.it", password: "asdafafa"})
      .then(function (res){
        var success = res.body.success;
        expect(success).to.equal(false);
        done();
      })
    });

    it("admin SIGNUP", function(done){
      chai.request(app)
      .post('/signup/business')
      .send(
        {
          name:"Nick",
          lastname:"Test",
          phone:"0220349586",
          address:"street 4",
          birthdate:"10/10/1980",
          email:"nick@gmail.com",
          password:"asdafafa"
        }
      )
      .end(function (err, res){
        var success = res.body.success;
        expect(err).to.be.null;
        expect(success).to.equal(true);
        done();
      })
    });

    it("Authentication Successful", function(done) {
      chai.request(app)
      .post('/auth/business')
      .send(
        {
          email:"nick@gmail.com",
          password:"asdafafa"
        }
      )
      .end(function (err, res){
        var success = res.body.success;
        expect(err).to.be.null;
        expect(success).to.equal(true);
        done();
      })
    });

    it("Authentication Without Body", function(done) {
      chai.request(app)
      .post('/auth/business')
      .end(function(err, res){
        var success=res.body.success;
        expect(success).to.equal(false);
        done();
      })
    });

  });

});*/
