var expect    = require("chai").expect;
var chai = require('chai');
var chaiHttp = require('chai-http');
var request = require("request");
var app = require('../app');
chai.use(chaiHttp);

describe('Authentication', function(){
  describe('Normal Authentication', function(){
    
    it("NORMAL SIGNUP", function(done){
      chai.request(app)
      .post('/signup/normal')
      .send(
        {
          name:"Nick",
          lastname:"Test",
          phone:"0220349586",
          address:"street 4",
          birthdate:"10/10/1980",
          email:"nick@gmail.com",
          password:"asdafafa"
        }
      )
      .end(function (err, res){
        var success = res.body.success;
        expect(err).to.be.null;
        expect(success).to.equal(true);
        done();
      })
    });
  })
});
