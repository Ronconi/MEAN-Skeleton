/*
var expect    = require("chai").expect;

var chai = require('chai');
var chaiHttp = require('chai-http');

process.env.NODE_ENV = 'test'

var app = require("../app");
var request = require("request");

chai.use(chaiHttp);

describe("Signup", function() {

  var url = "http://localhost:3000/signup/normal";

  describe("Normal Signup", function() {

    it("returns status 200", function(done) {
      request.post({url:url}, function(error, response, body) {
        expect(response.statusCode).to.equal(200);
        done();
      });
    });

    it("Signup Failed - Email already exists", function(done) {
      request.post({url:url, form:{name: "Nick", lastname: "Ronconi", email:"normal@gmail.it", password: "asdafafa", phone: "3400000000", address: "street n1", birthdate: "10/10/1980" }},
        function(error, response, body) {
          var body = JSON.parse(body);
          var success = body.success;
          expect(success).to.equal(false);
          done();
        }
      );
    });

    it("Signup Successful", function(done) {
      request.post({url:url, form:{name: "Nick", lastname: "Ronconi", email: faker.internet.email(), password: "asdafafa", phone: "3400000000", address: "street n1", birthdate: "10/10/1980" }},
        function(error, response, body) {
          var body = JSON.parse(body);
          var success = body.success;
          expect(success).to.equal(true);
          done();
        }
      );
    });

    it("Signup Without Body", function(done) {
      request.post({url:url, form:{}},
        function(error, response, body) {
          var body = JSON.parse(body);
          var success = body.success;
          expect(success).to.equal(false);
          done();
        }
      );
    });

  });

  var url_admin = "http://localhost:3000/signup/admin";

  describe("Admin Signup", function() {

    it("returns status 200", function(done) {
      request.post({url:url_admin,form:{},
      headers: {
      'Authorization':'JWT eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJfaWQiOiI1N2Q2ODU1YTkwYmE0MzBiYjMzOWQ5ZWIiLCJuYW1lIjoiTmljb2zDsiIsImxhc3RuYW1lIjoiUm9uY29uaSIsImVtYWlsIjoiYWRtaW5AZ21haWwuY29tIiwicGFzc3dvcmQiOiIkMmEkMTAkTGliTTZ4N2xMZnlXZURPbTYwbjdpLi5jRUlidVpjVnBDWWpHZGMyV25ERk9UZ2Z1Z2V3NzIiLCJncm91cCI6ImFkbWluIiwiX192IjowfQ.kpVBO9Jcy4NxIRuY7TtWidojTLVTDZ15dbaa9qFpSsA'
    }}, function(error, response, body) {
        expect(response.statusCode).to.equal(200);
        done();
      });
    });

    it("returns status 401", function(done) {
      request.post({url:url_admin}, function(error, response, body) {
        expect(response.statusCode).to.equal(401);
        done();
      });
    });

    it("Signup Failed - Email already exists", function(done) {
      request.post({url:url_admin, form:{email:"admin@gmail.it", password: "asdafafa", name: "Nick", lastname: "Ronconi"},
      headers: {
      'Authorization':'JWT eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJfaWQiOiI1N2Q2ODU1YTkwYmE0MzBiYjMzOWQ5ZWIiLCJuYW1lIjoiTmljb2zDsiIsImxhc3RuYW1lIjoiUm9uY29uaSIsImVtYWlsIjoiYWRtaW5AZ21haWwuY29tIiwicGFzc3dvcmQiOiIkMmEkMTAkTGliTTZ4N2xMZnlXZURPbTYwbjdpLi5jRUlidVpjVnBDWWpHZGMyV25ERk9UZ2Z1Z2V3NzIiLCJncm91cCI6ImFkbWluIiwiX192IjowfQ.kpVBO9Jcy4NxIRuY7TtWidojTLVTDZ15dbaa9qFpSsA'
      }},
        function(error, response, body) {
          var body = JSON.parse(body);
          var success = body.success;
          expect(success).to.equal(false);
          done();
        }
      );
    });

    it("Signup Successful", function(done) {
      request.post({url:url_admin, form:{email: faker.internet.email(), password: "asdafafa", name: "Nick", lastname: "Ronconi"},
      headers: {
      'Authorization':'JWT eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJfaWQiOiI1N2Q2ODU1YTkwYmE0MzBiYjMzOWQ5ZWIiLCJuYW1lIjoiTmljb2zDsiIsImxhc3RuYW1lIjoiUm9uY29uaSIsImVtYWlsIjoiYWRtaW5AZ21haWwuY29tIiwicGFzc3dvcmQiOiIkMmEkMTAkTGliTTZ4N2xMZnlXZURPbTYwbjdpLi5jRUlidVpjVnBDWWpHZGMyV25ERk9UZ2Z1Z2V3NzIiLCJncm91cCI6ImFkbWluIiwiX192IjowfQ.kpVBO9Jcy4NxIRuY7TtWidojTLVTDZ15dbaa9qFpSsA'
      }},
        function(error, response, body) {
          var body = JSON.parse(body);
          var success = body.success;
          expect(success).to.equal(true);
          done();
        }
      );
    });

    it("Signup Without Body", function(done) {
      request.post({url:url_admin, form:{},
      headers: {
      'Authorization':'JWT eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJfaWQiOiI1N2Q2ODU1YTkwYmE0MzBiYjMzOWQ5ZWIiLCJuYW1lIjoiTmljb2zDsiIsImxhc3RuYW1lIjoiUm9uY29uaSIsImVtYWlsIjoiYWRtaW5AZ21haWwuY29tIiwicGFzc3dvcmQiOiIkMmEkMTAkTGliTTZ4N2xMZnlXZURPbTYwbjdpLi5jRUlidVpjVnBDWWpHZGMyV25ERk9UZ2Z1Z2V3NzIiLCJncm91cCI6ImFkbWluIiwiX192IjowfQ.kpVBO9Jcy4NxIRuY7TtWidojTLVTDZ15dbaa9qFpSsA'
      }},
        function(error, response, body) {
          var body = JSON.parse(body);
          var success = body.success;
          expect(success).to.equal(false);
          done();
        }
      );
    });

  });

  var url_business = "http://localhost:3000/signup/business";

  describe("Business Signup", function() {

    it("returns status 200", function(done) {
      request.post({url:url_business,form:{},
      headers: {
      'Authorization':'JWT eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJfaWQiOiI1N2Q2ODU1YTkwYmE0MzBiYjMzOWQ5ZWIiLCJuYW1lIjoiTmljb2zDsiIsImxhc3RuYW1lIjoiUm9uY29uaSIsImVtYWlsIjoiYWRtaW5AZ21haWwuY29tIiwicGFzc3dvcmQiOiIkMmEkMTAkTGliTTZ4N2xMZnlXZURPbTYwbjdpLi5jRUlidVpjVnBDWWpHZGMyV25ERk9UZ2Z1Z2V3NzIiLCJncm91cCI6ImFkbWluIiwiX192IjowfQ.kpVBO9Jcy4NxIRuY7TtWidojTLVTDZ15dbaa9qFpSsA'
      }}, function(error, response, body) {
        expect(response.statusCode).to.equal(200);
        done();
      });
    });

    it("returns status 401", function(done) {
      request.post({url:url_business}, function(error, response, body) {
        expect(response.statusCode).to.equal(401);
        done();
      });
    });

    it("Signup Failed - Email already exists", function(done) {
      request.post({ url:url_business,
          form: {
            email:"business@gmail.com",
            password: "asdafafa",
            name: "Nick"
          },
          headers: {
          'Authorization':'JWT eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJfaWQiOiI1N2Q2ODU1YTkwYmE0MzBiYjMzOWQ5ZWIiLCJuYW1lIjoiTmljb2zDsiIsImxhc3RuYW1lIjoiUm9uY29uaSIsImVtYWlsIjoiYWRtaW5AZ21haWwuY29tIiwicGFzc3dvcmQiOiIkMmEkMTAkTGliTTZ4N2xMZnlXZURPbTYwbjdpLi5jRUlidVpjVnBDWWpHZGMyV25ERk9UZ2Z1Z2V3NzIiLCJncm91cCI6ImFkbWluIiwiX192IjowfQ.kpVBO9Jcy4NxIRuY7TtWidojTLVTDZ15dbaa9qFpSsA'
          }
        },
        function(error, response, body) {
          var body = JSON.parse(body);
          var success = body.success;
          expect(success).to.equal(false);
          done();
        }
      );
    });

    it("Signup Successful", function(done) {
      request.post({url:url_business, form:{email: faker.internet.email(), password: "asdafafa", name: "Nick"},
      headers: {
      'Authorization':'JWT eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJfaWQiOiI1N2Q2ODU1YTkwYmE0MzBiYjMzOWQ5ZWIiLCJuYW1lIjoiTmljb2zDsiIsImxhc3RuYW1lIjoiUm9uY29uaSIsImVtYWlsIjoiYWRtaW5AZ21haWwuY29tIiwicGFzc3dvcmQiOiIkMmEkMTAkTGliTTZ4N2xMZnlXZURPbTYwbjdpLi5jRUlidVpjVnBDWWpHZGMyV25ERk9UZ2Z1Z2V3NzIiLCJncm91cCI6ImFkbWluIiwiX192IjowfQ.kpVBO9Jcy4NxIRuY7TtWidojTLVTDZ15dbaa9qFpSsA'
      }},
        function(error, response, body) {
          var body = JSON.parse(body);
          var success = body.success;
          expect(success).to.equal(true);
          done();
        }
      );
    });

    it("Authentication Without Body", function(done) {
      request.post({url:url_business, form:{},
      headers: {
      'Authorization':'JWT eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJfaWQiOiI1N2Q2ODU1YTkwYmE0MzBiYjMzOWQ5ZWIiLCJuYW1lIjoiTmljb2zDsiIsImxhc3RuYW1lIjoiUm9uY29uaSIsImVtYWlsIjoiYWRtaW5AZ21haWwuY29tIiwicGFzc3dvcmQiOiIkMmEkMTAkTGliTTZ4N2xMZnlXZURPbTYwbjdpLi5jRUlidVpjVnBDWWpHZGMyV25ERk9UZ2Z1Z2V3NzIiLCJncm91cCI6ImFkbWluIiwiX192IjowfQ.kpVBO9Jcy4NxIRuY7TtWidojTLVTDZ15dbaa9qFpSsA'
      }},
        function(error, response, body) {
          var body = JSON.parse(body);
          var success = body.success;
          expect(success).to.equal(false);
          done();
        }
      );
    });

  });

});
*/
