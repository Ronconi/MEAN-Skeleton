//models
var user = require('../models/user');
var admin = require('../models/admin');
var business = require('../models/business');

var bcrypt = require('bcryptjs');

var signupHandler = function(req, res){
  switch(req.path){
    case '/normal':
      if(isBodyNotEmpty('normal', req.body)){
        signup('normal', req.body, function(err, user){
          if(err){
            var msg = err.name;
            if(err.code == 11000){
              msg = res.__('email.exists');
              res.json({success: false, code: err.code, msg: msg});
            }else{
              res.status(200).json({success: false});
            }
          } else {
            res.json({success: true, msg: res.__('user.created')});
          }
        });
      } else {
        res.json({success:false, msg: res.__("errors.common.empty_body")})
      }
      break;
    case '/admin':
      if(isBodyNotEmpty('admin', req.body)){
        signup('admin', req.body, function(err, user){
          if(err){
            var msg = err.name;
            if(err.code == 11000){
              msg = res.__('email.exists');
              res.json({success: false, code: err.code, msg: msg});
            }else{
              res.status(500).json({success: false});
            }
          } else {
            res.json({success: true, msg: res.__('user.created')});
          }
        })
      } else {
        res.json({success:false, msg: res.__("errors.common.empty_body")})
      }
      break;
    case '/business':
      if(isBodyNotEmpty('business', req.body)){
        signup('business', req.body, function(err, user){
          if(err){
            var msg = err.name;
            if(err.code == 11000){
              msg = res.__('email.exists');
              res.json({success: false, code: err.code, msg: msg});
            }else{
              res.status(500).json({success: false});
            }
          } else {
            res.json({success: true, msg: res.__('user.created')});
          }
        })
      } else {
        res.json({success:false, msg: res.__("errors.common.empty_body")})
      }
      break;
    default:
      res.status(404).json({success:false});
      break;
  }
};

var isBodyNotEmpty = function(type, body){
  switch(type){
    case 'normal':
      if(!body.name || !body.lastname  || !body.email  || !body.password
          || !body.address || !body.phone || !body.birthdate)
        return false;
      else
        return true;
      break;
    case 'admin':
    if(!body.name || !body.lastname  || !body.email  || !body.password)
      return false;
    else
      return true;
      break;
    case 'business':
      if(!body.name || !body.email  || !body.password)
        return false;
      else
        return true;
      break;
  }
}

var signup = function(type, body, cb) {
  //switch logic for each user groups
  switch(type){
    case 'normal':
      createUser('normal', body, function(u){
          user.insert(u, function(err){
            if(err) cb(err);
            else cb(null);
          });
      });
      break;
    case 'admin':
      createUser('admin', body, function(a){
          admin.insert(a, function(err){
            if(err) cb(err);
            else cb(null);
          });
      });
      break;
    case 'business':
      createUser('business', body, function(b){
          business.insert(b, function(err){
            if(err) cb(err);
            else cb(null);
          });
      });
      break;
    }
}

var createUser = function(type, body, cb){
  bcrypt.genSalt(10, function(err, salt){
      bcrypt.hash(body.password, salt, function(err, hash) {
        switch(type){
          case 'normal':
            var user = {
              name: body.name,
              lastname: body.lastname,
              email: body.email,
              password: hash,
              address: body.address,
              birthdate: body.birthdate,
              phone: body.phone,
              strategy: 'local'
            }
            cb(user);
            break;
          case 'admin':
            var admin = {
              name: body.name,
              email: body.email,
              password: hash,
            }
            cb(admin);
            break;
          case 'business':
            var business = {
              name: body.name,
              email: body.email,
              password: hash
            }
            cb(business);
            break;
        }
      });
  });
}

module.exports = {
  signup: signupHandler
}
