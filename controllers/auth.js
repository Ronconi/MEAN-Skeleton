//models
var user = require('../models/user');
var admin = require('../models/admin');
var business = require('../models/business');

var bcrypt = require('bcryptjs');
var jwt = require('jwt-simple');
var secret = require('../config/config').secret;


//function for handle the request
var authenticateHandler = function(req, res){
  var type = req.path.substring(1);
      if(isBodyNotEmpty(req.body)){ //check if request body is not empty
        authenticate( //if it's not, call authenticate function
          type, //for normal users
          req.body.email, //need email and pwd for authenticate a user
          req.body.password,
          function(err, user){ //Callback. Authenticated if user !empty
            if(err){ // if err !null
              if(err.code == 1) //wrong credentials
                res.status(401).json({success: false, msg:res.__('error.authentication')});
              else // server error
                res.status(500).json({success: false});
            } else { //send user info
              res.json({success: true, user: user});
            }
        });
      } else { // send error message. Body is empty
        res.json({success:false, msg: res.__("errors.common.empty_body")})
      }
};

//check if body is not empty
var isBodyNotEmpty = function(body){
  if(!body.email  || !body.password)
    return false;
  else
    return true;
}

var authenticate = function(type, email, password, cb){
  switch(type){
    case 'normal':
      user.findByEmail(email, function(err, user){
        if(err) { cb(err) }
        else {
          if(user){
            comparePassword(password, user.password, function(err, isMatch){
              if(err){
                cb(err, null)
              } else {
                if(isMatch){
                  createUser('normal', user, password, function(err, user){
                    if(err) cb(err);
                    else cb(null, user);
                  })
                }else{
                  cb({code:1});
                }
              }
            })
          }else{
            cb({code:1});
          }
        }
      });
      break;
    case 'admin':
      admin.findByEmail(email, function(err, user){
        if(err) { cb(err) }
        else {
          if(user){
            comparePassword(password, user.password, function(err, isMatch){
              if(err){
                cb(err, null)
              } else {
                if(isMatch){
                  createUser('admin', user, password, function(err, user){
                    if(err) cb(err);
                    else cb(null, user);
                  })
                }else{
                  cb({code:1});
                }
              }
            })
          }else{
            cb({code:1});
          }
        }
      });
      break;
    case 'business':
      business.findByEmail(email, function(err, user){
        if(err) { cb(err) }
        else {
          if(user){
            comparePassword(password, user.password, function(err, isMatch){
              if(err){
                cb(err, null)
              } else {
                if(isMatch){
                  createUser('business', user, password, function(err, user){
                    if(err) cb(err);
                    else cb(null, user);
                  })
                }else{
                  cb({code:1});
                }
              }
            })
          }else{
            cb({code:1});
          }
        }
      });
      break;
  }
}

var comparePassword = function(x, y, cb){
  bcrypt.compare(x, y, function(err, isMatch){
    if(err)
      cb(err, null)
    if(isMatch){
      cb(null, true)
    } else {
      cb(null, false)
    }
  })
}

var createUser = function(type, user, pwd, cb){
    var token = 'JWT ' + jwt.encode(user, secret);
    switch(type){
      case 'normal':
        var user = {
          _id: user._id,
          name: user.name,
          lastname: user.lastname,
          email: user.email,
          address: user.address,
          phone: user.phone,
          birthdate: user.birthdate,
          strategy: user.strategy,
          token: token
        }
        cb(null, user)
        break;
      case 'admin':
        var user = {
          _id: user._id,
          name: user.name,
          lastname: user.lastname,
          email: user.email,
          group: 'admin',
          token: token
        }
        cb(null, user)
        break;
      case 'business':
        var user = {
          _id: user._id,
          name: user.name,
          email: user.email,
          token: token
        }
        cb(null, user)
        break;
    }
  }

var getToken = function(headers) {
  if(headers && headers.authorization) {
    var parted = headers.authorization.split(' ');
    if(parted.length === 2)
      return parted[1];
    else
      return null;
  } else {
    return null;
  }
}

module.exports = {
  authenticate: authenticateHandler
}
